# blazor-wasm-bff

Blazor WASM hosted with BFF by Identity Server

This is an up to date version for https://github.com/DuendeSoftware/Samples/tree/main/IdentityServer/v6/BFF/BlazorWasm but upgraded to latest libraries and .NET 7

In addition, it has remote API support. See https://docs.duendesoftware.com/identityserver/v6/bff/apis/remote/
so it uses Microsoft YARP dependency to forward certain routes in BFF to remote APIs. 
These routes have the same anti-forgery protection as local API endpoints and also integrate with the automatic token management.

## How to run
Launch BlazorWasm.Shared